package com.tarion.graphservice.controller;

import java.util.logging.Level;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.microsoft.graph.models.Event;
import com.microsoft.graph.requests.CalendarGetScheduleCollectionPage;
import com.microsoft.graph.requests.EventCollectionPage;
import com.microsoft.graph.requests.UserCollectionPage;
import com.tarion.graphservice.dto.AvailableTimeslotsRequest;
import com.tarion.graphservice.dto.AvailableTimeslotsResponse;
import com.tarion.graphservice.dto.BookedTimeslotsRequest;
import com.tarion.graphservice.dto.CreateAppointmentRequest;
import com.tarion.graphservice.dto.CreateAppointmentResponse;
import com.tarion.graphservice.dto.DeleteAppointmentRequest;
import com.tarion.graphservice.dto.DeleteAppointmentResponse;
import com.tarion.graphservice.dto.GetSchedulesRequest;
import com.tarion.graphservice.service.GraphService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.java.Log;

@Log
@RestController
@RequestMapping("/GraphService")
@Api(tags = {"GraphService"})
public class GraphServiceController {
	
	@GetMapping(value = "/")
    public String getIndex() {
		log.log(Level.INFO, "Graph Service called");
        return "Graph Service";
    }
	
	@GetMapping(value = "/getUsers")
    public ResponseEntity<UserCollectionPage> getUsers() {
		log.log(Level.INFO, "getUsers called");
		UserCollectionPage users = new GraphService().getUsers();
		return new ResponseEntity<>(users, HttpStatus.OK);
    }

	@GetMapping(value = "/getCalendarView")
    public ResponseEntity<EventCollectionPage> getCalendarView(@RequestBody  GetSchedulesRequest request) {
		log.log(Level.INFO, "getCalendarView called");
		EventCollectionPage calendarView = new GraphService().getCalendarView(request.getFcrUsernames().get(0), request.getStartDate(), request.getEndDate());
		return new ResponseEntity<>(calendarView, HttpStatus.OK);
    }

    

    @PostMapping(value = "/getBookedTimeslots", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Search Outlook for booked timeslots", nickname = "getBookedTimeslots")
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of booked timeslots"),
            @ApiResponse(code = 204, message = "No  booked timeslots found", response = void.class),
            @ApiResponse(code = 400, message = "Invalid or Malformed request", response = void.class)
    })

    public ResponseEntity<EventCollectionPage> getBookedTimeslots(@RequestBody  BookedTimeslotsRequest request) {
    	log.log(Level.INFO, "getBookedTimeslots: " + request);
    	//BookedTimeslotsResoponse response = new BookedTimeslotsResoponse();
    	EventCollectionPage events = new GraphService().getEvents(request.getFcrUsernames().get(0), request.getStartDate(), request.getEndDate());
        return new ResponseEntity<>(events, HttpStatus.OK);
    }
    
    @PostMapping(value = "/getSchedules", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Search calendars", nickname = "getBookedTimeslots")
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of users' calendars"),
            @ApiResponse(code = 204, message = "No  calendars found", response = void.class),
            @ApiResponse(code = 400, message = "Invalid or malformed request", response = void.class)
    })

    public ResponseEntity<CalendarGetScheduleCollectionPage> getSchedules(@RequestBody  GetSchedulesRequest request) {
    	log.log(Level.INFO, "1 getSchedules called: " + request);
    	
    	CalendarGetScheduleCollectionPage response = new GraphService().getSchedules(request.getFcrUsernames().get(0), request.getStartDate(), request.getEndDate());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    
    @PostMapping(value = "/getAvailableTimeslots", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Search available timeslots", nickname = "getAvailableTimeslots")
    @ApiResponses({
            @ApiResponse(code = 200, message = "List of users' calendars"),
            @ApiResponse(code = 204, message = "No  calendars found", response = void.class),
            @ApiResponse(code = 400, message = "Invalid or malformed request", response = void.class)
    })

    public ResponseEntity<AvailableTimeslotsResponse> getAvailableTimeslots(@RequestBody  AvailableTimeslotsRequest request) {
    	log.log(Level.INFO, "getAvailableTimeslots called: " + request);  	
    	AvailableTimeslotsResponse response = new GraphService().getAvailableTimeslots(request.getFcrUsernames(), request.getStartDate(), request.getEndDate(), request.getAppointmentType(), request.isMultipleFCRs());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


	@PostMapping(value = "/createAppointment", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create new appointment", nickname = "createAppointment")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Appointment created"),
            @ApiResponse(code = 204, message = "Error", response = void.class),
            @ApiResponse(code = 400, message = "Invalid or Malformed request", response = void.class)
    })

    public ResponseEntity<CreateAppointmentResponse> createAppointment(@RequestBody  CreateAppointmentRequest request) {
    	CreateAppointmentResponse response = new CreateAppointmentResponse();
    	Event newEvent = new GraphService().createAppointment(request);
    	response.setResult("Success");
    	response.setEventId(newEvent.id);
    	response.setTopic(newEvent.subject);
    	response.setBody(newEvent.body.content);
    	response.setStartDateTime(request.getStartDateTimeStr());
    	response.setEndDateTime(request.getEndDateTimeStr());
    	response.setFcrId(request.getFcrId());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
	
	@PostMapping(value = "/deleteAppointment", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete appointment", nickname = "deleteAppointment")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Appointment deleted"),
            @ApiResponse(code = 204, message = "Error", response = void.class),
            @ApiResponse(code = 400, message = "Invalid or Malformed request", response = void.class)
    })

    public ResponseEntity<DeleteAppointmentResponse> deleteAppointment(@RequestBody  DeleteAppointmentRequest request) {
		log.log(Level.INFO, "GraphServiceController DeleteAppointmentRequest: " + request);
		DeleteAppointmentResponse response = new DeleteAppointmentResponse();
    	new GraphService().deleteEvent(request);
    	response.setResult("Success");
    	response.setGraphEventId(request.getGraphAppointmentId());
    	response.setMessage("Event with ID: " + request.getGraphAppointmentId() + " deleted");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}

package com.tarion.graphservice.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import lombok.extern.java.Log;

@Log
public class BasicAuthorizationFilter extends BasicAuthenticationFilter {


    public BasicAuthorizationFilter(AuthenticationManager authManager) {
        super(authManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader("Authorization");
        log.log(Level.INFO, "header: " + header);
        if (header == null || !header.startsWith("Basic")) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }
    
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String usernamePassword = request.getHeader("Authorization").split(" ")[1];
        String[] credentials = usernamePassword.split(":");
        log.log(Level.INFO, "username: " + credentials[0]);
        log.log(Level.INFO, "password: " + credentials[1]);
        if(credentials[0].equals("GraphUser") && credentials[1].equals("GraphService")) {
        	 List<String> permissions = new ArrayList<>();
             String user = "USER";
             permissions.add(user);
             if (user != null) {
                 UsernamePasswordAuthenticationToken t = new UsernamePasswordAuthenticationToken(user, null,
                         permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
                 log.log(Level.INFO, "Verified user");
                 return t;
             }
        	
        }

        return null;
    }

}
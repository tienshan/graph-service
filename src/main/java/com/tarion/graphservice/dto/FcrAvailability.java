package com.tarion.graphservice.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel
@Data
public class FcrAvailability {
    private String emailAddress;
    private List<Timeslot> availableTimeslots;

}

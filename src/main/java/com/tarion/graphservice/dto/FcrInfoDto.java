package com.tarion.graphservice.dto;

import lombok.Data;

@Data
public class FcrInfoDto {
	
	private String frcEmailAddress;
	private String fcrUsername;
	private String fcrGraphId;

}

package com.tarion.graphservice.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

@ApiModel
@ToString(includeFieldNames=true)
@Data
public class CreateAppointmentResponse {

    String result;
    String fcrId;
    String eventId;
    String topic;
    String body;
    String startDateTime;
    String endDateTime;

}

package com.tarion.graphservice.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel
@Data
public class BookedTimeslotsRequest {

    private List<String> fcrUsernames;
    private String startDate;
    private String endDate;

}

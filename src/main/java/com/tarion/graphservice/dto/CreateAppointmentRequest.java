package com.tarion.graphservice.dto;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

@ApiModel
@ToString(includeFieldNames=true)
@Data
public class CreateAppointmentRequest {
	String startDateTimeStr;
	String endDateTimeStr;
	String fcrId;
	String topic;
	String body;
	String inspectionType;
	

}

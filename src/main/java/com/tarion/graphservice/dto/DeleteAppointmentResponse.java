package com.tarion.graphservice.dto;


import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

@ApiModel
@ToString(includeFieldNames=true)
@Data
public class DeleteAppointmentResponse {
	String graphEventId;
	String result;
	String message;
}

package com.tarion.graphservice.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TwoFcrTimeslot {

	private LocalDateTime starttime;
    private LocalDateTime endtime;
    private String fcr1EmailAddress;
    private String fcr2EmailAddress;
    private String fcr1Username;
    private String fcr2Username;
    private String enrolmentAddress;
    private String appointmentType;
  

}

package com.tarion.graphservice.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel
@Data
public class AvailableTimeslotsResponse {
    private List<Timeslot> availabletimeslots;
    private List<Timeslot> inspectionsList;

}

package com.tarion.graphservice.dto;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

@ApiModel
@ToString(includeFieldNames=true)
@Data
public class BookedTimeslotsResoponse {

    private Map<String, List<Timeslot>> timeslotsMap ;

}

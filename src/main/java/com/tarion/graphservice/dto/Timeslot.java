package com.tarion.graphservice.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class Timeslot {

	private LocalDateTime starttime;
    private LocalDateTime endtime;
    private String emailAddress;
    private String fcrid;
    private String enrolmentAddress;
    private String appointmentType;
  

}

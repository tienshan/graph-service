package com.tarion.graphservice.config;

import java.lang.reflect.Field;
import java.security.Principal;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Aspect
@Component
public class LoggingConfig {

    public static final String USER_ID = "User id";
    public static final String CLASS_NAME = "Class name";
    public static final String METHOD_NAME = "Method name";
    public static final String REQUEST_PARAMETERS = "Request parameters";
    public static final String EXECUTION_TIME = "Execution time";
    public static final String RESPONSE_SUMMARY = "Response summary";
    public static final String RESPONSE = "Response";

    private static final Logger log = LoggerFactory.getLogger(LoggingConfig.class);

    private static final String[] ID_FIELD_NAMES = {
            "id",
            "cmItemId",
            "externalId",
            "enrolmentNumber",
            "vbNumber",
            "recordId"
    };

    private final ObjectMapper mapper;

    @Autowired
    public LoggingConfig(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Around("within(com.tarion.graphservice.controller..*) || within(com.tarion.graphservice.service..*) || within(com.tarion.graphservice.security..*)")
    public Object createEntryAndExitLogs(ProceedingJoinPoint point) throws Throwable {
        try {
            createEntryLog(point);
        } catch (Exception e) {
            log.error("Error when creating entry log in " + point.getSignature().getDeclaringTypeName());
        }

        long startTime = System.currentTimeMillis();
        Object object = point.proceed();
        long endTime = System.currentTimeMillis();

        try {
            createExitLog(point, endTime - startTime, object);
        } catch (Exception e) {
            log.error("Error when creating exit log in " + point.getSignature().getDeclaringTypeName());
        }
        return object;
    }

    private void createEntryLog(ProceedingJoinPoint point) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(USER_ID, SecurityContextHolder.getContext().getAuthentication().getName());
        map.put(CLASS_NAME, point.getSignature().getDeclaringTypeName());
        map.put(METHOD_NAME, point.getSignature().getName());
        map.put(REQUEST_PARAMETERS, getParametersInfo(point));
        log.info(formatLogMessage(map, true));
    }

    private void createExitLog(ProceedingJoinPoint point, long execTime, Object returnObject) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put(USER_ID, SecurityContextHolder.getContext().getAuthentication().getName());
        map.put(CLASS_NAME, point.getSignature().getDeclaringTypeName());
        map.put(METHOD_NAME, point.getSignature().getName());
        map.put(EXECUTION_TIME, execTime + "ms");
        if (returnObject instanceof ResponseEntity) {
            map.put(RESPONSE_SUMMARY, getResponseSummary(returnObject));
        }
//        if (log.isDebugEnabled()) {
        map.put(RESPONSE, returnObject.toString());
//        }

        log.info(formatLogMessage(map, false));
    }

    private String getParametersInfo(ProceedingJoinPoint point) {
        List<Object> args = Arrays.asList(point.getArgs());
        if (!CollectionUtils.isEmpty(args)) {
            StringBuilder b = new StringBuilder();
            String prefix = "";
            for (Object arg : args) {
                if (arg instanceof HttpServletRequest || arg instanceof Principal) {
                    continue;
                }
                b.append(prefix);
                prefix = ", ";
                b.append(getString(arg));
            }
            return b.toString();
        } else {
            return null;
        }
    }

    private String getString(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return object.toString();
        }
    }

    private String getResponseSummary(Object returnObject) {
        ResponseEntity<?> response = (ResponseEntity<?>) returnObject;
        Object body = response.getBody();
        if (body == null) {
            return null;
        } else if (body instanceof List<?>) {
            return getListSummary((List<?>) body);
        } else {
            return getSingleObjectSummary(body);
        }
    }

    private String getListSummary(List<?> objectList) {
        if (CollectionUtils.isEmpty(objectList)) {
            return null;
        }
        StringBuilder b = new StringBuilder("List of " + objectList.size() + " " + objectList.get(0).getClass().getSimpleName() + " with ids: [");
        String prefix = "";
        for (Object obj : objectList) {
            b.append(prefix);
            prefix = ",";
            b.append(getObjectId(obj));
        }
        b.append("]");
        return b.toString();
    }

    private String getSingleObjectSummary(Object object) {
        return object.getClass().getSimpleName() + " with id: " + getObjectId(object);
    }

    private String getObjectId(Object object) {
        for (String idName : ID_FIELD_NAMES) {
            Field field = getField(object, idName);
            if (field != null) {
                try {
                    field.setAccessible(true);
                    return field.getName() + ":" + field.get(object);
                } catch (Exception e) {
                    return null;
                }
            }
        }
        return null;
    }

    private Field getField(Object object, String fieldName) {
        try {
            return object.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            return null;
        }
    }

    private String formatLogMessage(Map<String, String> map, boolean isEntryLog) {
        String firstLine = isEntryLog ? "Entry log: " : "Exit log: ";
        StringBuilder b = new StringBuilder(firstLine).append(map);
        return b.toString();
    }

}

package com.tarion.graphservice.util;

public class GraphServiceConstants {
	
	public static final String AM_PM_TIMESLOT = "AM/PM";
	public static final String ALL_DAY_TIMESLOT = "AllDay";
	public static final String MULTIPLE_DAYS_TIMESLOT = "MultipleDays";
	public static final String TWO_FCR_TIMESLOT = "TwoFcr";
	public static final String BUSY_TIMESLOT =  "BUSY";
	public static final String FREE_TIMESLOT =  "FREE";
	

}

package com.tarion.graphservice.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import com.microsoft.graph.models.Event;
import com.microsoft.graph.models.FreeBusyStatus;
import com.microsoft.graph.models.ScheduleInformation;
import com.microsoft.graph.models.ScheduleItem;
import com.microsoft.graph.requests.CalendarGetScheduleCollectionPage;
import com.microsoft.graph.requests.EventCollectionPage;
import com.tarion.graphservice.dto.Timeslot;
import com.tarion.graphservice.service.GraphService;

import lombok.extern.java.Log;

@Log
public class TimeslotUtil {
	
	private static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String DATE_TIME_PATTERN_GRAPH = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS";
	public static final String DATE_PATTERN_DATE_ONLY = "yyyy-MM-dd";
	GraphService graphService = new GraphService();
	
	public List<Timeslot> getAvailableAllDaysTimeslots(String fcrUsername, String startTimeString, String endTimeString,
			EventCollectionPage events) {
		log.log(Level.INFO, "getAvailableAllDaysTimeslots with events, startDateTime=" + startTimeString + ", endDateTime=" + endTimeString + ", frcUsername" + fcrUsername);
		
		List<Timeslot> timeslotsToRemove = new ArrayList<>();
		List<Timeslot> potentialTimeslots = getPotentialAllDayTimeslots(startTimeString, endTimeString, fcrUsername);

		for (int i = 0; i < events.getCurrentPage().size(); i++) {
			 try {
				 Event event = events.getCurrentPage().get(i);
				 for(Timeslot potentialTimeslot : potentialTimeslots) {
					 if(isOverlapping(event, potentialTimeslot)){
						 timeslotsToRemove.add(potentialTimeslot); 
					 }
				 }
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		 }
		 List<Timeslot> availableTimeslots = getAvailableTimeslots(potentialTimeslots, timeslotsToRemove);
		return availableTimeslots;
	}

	
	public List<Timeslot> getAvailableAMPMTimeslots(String fcrUsername, String startDateTime, String endDateTime, EventCollectionPage events){
		
		log.log(Level.INFO, "getAvailableAMPMTimeslots with events, startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + ", frcUsername" + fcrUsername);
		
		List<Timeslot> timeslotsToRemove = new ArrayList<>();
		List<Timeslot> potentialTimeslots = getPotentialAMPMTimeslots(startDateTime, endDateTime, fcrUsername);

		for (int i = 0; i < events.getCurrentPage().size(); i++) {
			 try {
				 Event event = events.getCurrentPage().get(i);
				 if(event.showAs.equals(FreeBusyStatus.FREE)) {
					 //do not process free timeslots
					 continue;
				 }
				 for(Timeslot potentialTimeslot : potentialTimeslots) {
					 if(isOverlapping(event, potentialTimeslot)){
						 timeslotsToRemove.add(potentialTimeslot); 
					 }
				 }
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		 }
		 List<Timeslot> availableTimeslots = getAvailableTimeslots(potentialTimeslots, timeslotsToRemove);
		return availableTimeslots;
	}
	
	public List<Timeslot> getAvailableMultipleDaysTimeslots(String fcrUsername, String startTimeString, String endTimeString,
			EventCollectionPage events) {
		log.log(Level.INFO, "getAvailableMultipleDaysTimeslots with events, startDateTime=" + startTimeString + ", endDateTime=" + endTimeString + ", frcUsername" + fcrUsername);
		
		List<Timeslot> timeslotsToRemove = new ArrayList<>();
		List<Timeslot> potentialTimeslots = getPotentialMultipleDaysTimeslots(startTimeString, endTimeString, fcrUsername);

		for (int i = 0; i < events.getCurrentPage().size(); i++) {
			 try {
				 Event event = events.getCurrentPage().get(i);
				 for(Timeslot potentialTimeslot : potentialTimeslots) {
					 if(isOverlapping(event, potentialTimeslot)){
						 timeslotsToRemove.add(potentialTimeslot); 
					 }
				 }
			 } catch (Exception e) {
				e.printStackTrace();
			 }
		 }
		 List<Timeslot> availableTimeslots = getAvailableTimeslots(potentialTimeslots, timeslotsToRemove);
		return availableTimeslots;
	}
	
	public List<Timeslot> getAvailableAMPMTimeslots(String fcrUsername, String startDateTime, String endDateTime, CalendarGetScheduleCollectionPage schedules){
		
		log.log(Level.INFO, "getAvailableAMPMTimeslots, startDateTime=" + startDateTime + ", endDateTime=" + endDateTime + ", frcUsername" + fcrUsername);
		
		List<Timeslot> timeslotsToRemove = new ArrayList<>();
		List<Timeslot> potentialTimeslots = getPotentialAMPMTimeslots(startDateTime, endDateTime, fcrUsername);

		 for (int i = 0; i < schedules.getCurrentPage().size(); i++) {
			 log.log(Level.INFO, "schedules.getCurrentPage(): " + schedules.getCurrentPage());
			 log.log(Level.INFO, "schedules.getCurrentPage().size(): " + schedules.getCurrentPage().size());
			 try {
				ScheduleInformation scheduleInformation = schedules.getCurrentPage().get(i);
				log.log(Level.INFO, "scheduleInformation: " + scheduleInformation);
				 List<ScheduleItem> scheduledItems = scheduleInformation.scheduleItems;
				 if(scheduledItems != null) {// important - this can be null
					 for(ScheduleItem scheduledItem : scheduledItems) {
						 if(scheduledItem.status.equals(FreeBusyStatus.FREE)) {
							 //do not process free timeslots
							 continue;
						 }
						 for(Timeslot potentialTimeslot : potentialTimeslots) {
							 if(isOverlapping(scheduledItem, potentialTimeslot)){
								 timeslotsToRemove.add(potentialTimeslot); 
							 }
						 }
					 }					 
				 }
			} catch (Exception e) {
				log.log(Level.INFO, "getAvailableAMPMTimeslots: " + e.getMessage());
				e.printStackTrace();
			}
		 }
		 List<Timeslot> availableTimeslots = getAvailableTimeslots(potentialTimeslots, timeslotsToRemove);
		return availableTimeslots;
	}

	private List<Timeslot> getAvailableTimeslots(List<Timeslot> potentialTimeslots, List<Timeslot> timeslotsToRemove) {
		for(Timeslot timeslot: timeslotsToRemove) {
			potentialTimeslots.remove(timeslot);
		}
		return potentialTimeslots;
	}

	private boolean isOverlapping(ScheduleItem scheduledItem, Timeslot potentialTimeslot) {
		String startDateTimeTimeZone =  scheduledItem.start.dateTime;
		String endDateTimeTimeZone =  scheduledItem.end.dateTime;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_GRAPH);
		LocalDateTime scheduledItemStartDateTime = LocalDateTime.parse(startDateTimeTimeZone, formatter);
		LocalDateTime scheduledItemEndDateTime = LocalDateTime.parse(endDateTimeTimeZone, formatter);
		if(scheduledItemStartDateTime.isBefore(potentialTimeslot.getEndtime()) && potentialTimeslot.getStarttime().isBefore(scheduledItemEndDateTime)) {
			return true;
		}
		return false;
	}
	
	private boolean isOverlapping(Event event, Timeslot potentialTimeslot) {
		String startDateTimeTimeZone =  event.start.dateTime;
		String endDateTimeTimeZone =  event.end.dateTime;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_GRAPH);
		LocalDateTime scheduledItemStartDateTime = LocalDateTime.parse(startDateTimeTimeZone, formatter);
		LocalDateTime scheduledItemEndDateTime = LocalDateTime.parse(endDateTimeTimeZone, formatter);
		if(scheduledItemStartDateTime.isBefore(potentialTimeslot.getEndtime()) && potentialTimeslot.getStarttime().isBefore(scheduledItemEndDateTime)) {
			return true;
		}
		return false;
	}

	private List<Timeslot> getPotentialAMPMTimeslots(String startDateTime, String endDateTime, String frcUsername) {
		log.log(Level.INFO, "getPotentialAMPMTimeslots for startDateTime=" + startDateTime + ", endDateTime" + endDateTime + ", frcUsername" + frcUsername);
		List<Timeslot> potentialTimeslots = new ArrayList<>();
		List<LocalDate> datesList = getDatesBetween(startDateTime, endDateTime);
		//get 10:00 - 12:30 timeslots
		for(LocalDate date : datesList) {
			LocalDateTime startTimeTimeslotAM = date.atTime(10,0);
			LocalDateTime endTimeTimeslotAM = date.atTime(12,30);
			Timeslot timeslot = new Timeslot();
			timeslot.setStarttime(startTimeTimeslotAM);
			timeslot.setEndtime(endTimeTimeslotAM);
			timeslot.setFcrid(frcUsername);
			potentialTimeslots.add(timeslot);
		}

		//get 14:00 - 16:30 timeslots
		for(LocalDate date : datesList) {			
			LocalDateTime startTimeTimeslotPM = date.atTime(14,0);
			LocalDateTime endTimeTimeslotPM = date.atTime(16,30);
			Timeslot timeslot = new Timeslot();
			timeslot.setStarttime(startTimeTimeslotPM);
			timeslot.setEndtime(endTimeTimeslotPM);
			timeslot.setFcrid(frcUsername);
			potentialTimeslots.add(timeslot);			
		}
		return potentialTimeslots;
	}
	
	private List<Timeslot> getPotentialMultipleDaysTimeslots(String startDateTimeStr, String endDateTimeStr, String frcUsername) {
		log.log(Level.INFO, "getPotentialMultipleDaysTimeslots for startDateTime=" + startDateTimeStr + ", endDateTime" + endDateTimeStr + ", frcUsername" + frcUsername);
		List<Timeslot> potentialTimeslots = new ArrayList<>();
		List<LocalDate> datesList = getDatesBetween(startDateTimeStr, endDateTimeStr);
		
		for(int i = 0; i<datesList.size(); i++) {
			LocalDate startDate = datesList.get(i);
			LocalDateTime startDateTime = startDate.atTime(8,30);
			i++;
			//move to next date
			LocalDate endtDate = datesList.get(i);
			LocalDateTime endDateTime = endtDate.atTime(8,30);
			Timeslot timeslot = new Timeslot();
			timeslot.setStarttime(startDateTime);
			timeslot.setEndtime(endDateTime);
			timeslot.setFcrid(frcUsername);
			potentialTimeslots.add(timeslot);
		}
		return potentialTimeslots;
	}
	
	private List<Timeslot> getPotentialAllDayTimeslots(String startDateTime, String endDateTime, String frcUsername) {
		log.log(Level.INFO, "getPotentialAllDayTimeslots for startDateTime=" + startDateTime + ", endDateTime" + endDateTime + ", frcUsername" + frcUsername);
		List<Timeslot> potentialTimeslots = new ArrayList<>();
		List<LocalDate> datesList = getDatesBetween(startDateTime, endDateTime);
		for(LocalDate date : datesList) {
			LocalDateTime startTimeTimeslotAM = date.atTime(8,30);
			LocalDateTime endTimeTimeslotAM = date.atTime(17,00);
			Timeslot timeslot = new Timeslot();
			timeslot.setStarttime(startTimeTimeslotAM);
			timeslot.setEndtime(endTimeTimeslotAM);
			timeslot.setFcrid(frcUsername);
			potentialTimeslots.add(timeslot);
		}
		return potentialTimeslots;
	}
	
	public static List<LocalDate> getDatesBetween(String startDateTime, String endDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
		LocalDate startDate = LocalDate.parse(startDateTime, formatter);
		LocalDate endDate = LocalDate.parse(endDateTime, formatter);		
		List<LocalDate> datesList =  startDate.datesUntil(endDate)
			      .collect(Collectors.toList());
		return datesList;
	}
	
	public static String getFormattedDateTimeStringForAvailableTimeslots(String dateTimeString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN_DATE_ONLY);
		LocalDate startDate = LocalDate.parse(dateTimeString, formatter);		
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
		String formattedDateTime = startDate.format(dateTimeFormatter);
		return formattedDateTime;
	}
	
	public List<Timeslot> getInspectionsList(CalendarGetScheduleCollectionPage schedules) {
		List<Timeslot> inspectionsList = new ArrayList<>();
		for (int i = 0; i < schedules.getCurrentPage().size(); i++) {
			 log.log(Level.INFO, "getInspectionsList SCHEDULES: " + schedules.getCurrentPage());
			 try {
				ScheduleInformation scheduleInformation = schedules.getCurrentPage().get(i);
				 List<ScheduleItem> scheduledItems = scheduleInformation.scheduleItems;
				 if(scheduledItems != null) {// important - this can be null
					 for(ScheduleItem scheduledItem : scheduledItems) {
						 log.log(Level.INFO, "scheduleInformation: " + scheduleInformation);
						 String subject = scheduledItem.subject;
						 if(subject.indexOf("Conciliation inspection") != -1) {
						 Timeslot timeslot = createTimeslot(scheduledItem, scheduleInformation.scheduleId);							 
							 inspectionsList.add(timeslot);							 
						 }
					 }					 
				 }
			} catch (Exception e) {
				log.log(Level.INFO, "Error in 5: " + e.getMessage());
				e.printStackTrace();
			}

		 }
		 log.log(Level.INFO, "inspectionsList=" + inspectionsList);
		return inspectionsList;
	}
	
	public List<Timeslot> getInspectionsList(EventCollectionPage events, String fcrUsername) {
		List<Timeslot> inspectionsList = new ArrayList<>();
		for (int i = 0; i < events.getCurrentPage().size(); i++) {
			 log.log(Level.INFO, "getInspectionsList for EventCollectionPage: " + events.getCurrentPage());
			 try {
				 Event event = events.getCurrentPage().get(i);
				 String eventBody = event.bodyPreview;
				 if(eventBody.indexOf("FCR: ") != -1) {
					Timeslot timeslot = createTimeslot(event, graphService.getFcrEmailByUsername(fcrUsername));							 
					inspectionsList.add(timeslot);							 
				 }
			} catch (Exception e) {
				log.log(Level.INFO, "Error in 5: " + e.getMessage());
				e.printStackTrace();
			}

		 }
		 log.log(Level.INFO, "inspectionsList=" + inspectionsList);
		return inspectionsList;
	}



	private Timeslot createTimeslot(ScheduleItem scheduledItem, String emailAddress) {
		 Timeslot timeslot = new Timeslot();
		 String startDateTimeTimeZone =  scheduledItem.start.dateTime;
		 String endDateTimeTimeZone =  scheduledItem.end.dateTime;
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_GRAPH);
		 LocalDateTime scheduledItemStartDateTime = LocalDateTime.parse(startDateTimeTimeZone, formatter);
		 LocalDateTime scheduledItemEndDateTime = LocalDateTime.parse(endDateTimeTimeZone, formatter);

		 timeslot.setEmailAddress(emailAddress);
		 timeslot.setStarttime(scheduledItemStartDateTime);
		 timeslot.setEndtime(scheduledItemEndDateTime);
		 timeslot.setFcrid(graphService.getFrcUsername(emailAddress));
		 timeslot.setEnrolmentAddress(scheduledItem.subject.substring(scheduledItem.subject.indexOf(':')+ 2));		 
		 return timeslot;		 
	}	
	
	private Timeslot createTimeslot(Event event, String emailAddress) {
		 Timeslot timeslot = new Timeslot();
		 String startDateTimeTimeZone =  event.start.dateTime;
		 String endDateTimeTimeZone =  event.end.dateTime;
		 DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN_GRAPH);
		 LocalDateTime scheduledItemStartDateTime = LocalDateTime.parse(startDateTimeTimeZone, formatter);
		 LocalDateTime scheduledItemEndDateTime = LocalDateTime.parse(endDateTimeTimeZone, formatter);

		 timeslot.setEmailAddress(emailAddress);
		 timeslot.setStarttime(scheduledItemStartDateTime);
		 timeslot.setEndtime(scheduledItemEndDateTime);
		 timeslot.setFcrid(graphService.getFrcUsername(emailAddress));
		 timeslot.setEnrolmentAddress(event.bodyPreview.substring(event.bodyPreview.indexOf("Address: " + 9)));
		 log.log(Level.INFO, "Inspection address=" + timeslot.getEnrolmentAddress());
		 return timeslot;		 
	}





}

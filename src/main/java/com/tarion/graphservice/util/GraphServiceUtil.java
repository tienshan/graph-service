package com.tarion.graphservice.util;

import java.text.ParseException;
import java.util.GregorianCalendar;
import java.util.Locale;

public class GraphServiceUtil {
	

	public static GregorianCalendar createGregorianCalendar(String aDate, String datePattern) throws ParseException {
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat(datePattern, Locale.CANADA);
		java.util.Date date = format.parse(aDate);

		GregorianCalendar greCal = new GregorianCalendar(Locale.CANADA);
		greCal.setTime(date);
		return greCal;
	}

}

package com.tarion.graphservice.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import com.azure.identity.ClientSecretCredential;
import com.azure.identity.ClientSecretCredentialBuilder;
//import com.microsoft.graph.auth.enums.NationalCloud;
//import com.microsoft.graph.auth.publicClient.UsernamePasswordProvider;
import com.microsoft.graph.authentication.TokenCredentialAuthProvider;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.models.Attendee;
import com.microsoft.graph.models.AttendeeType;
import com.microsoft.graph.models.BodyType;
import com.microsoft.graph.models.CalendarGetScheduleParameterSet;
import com.microsoft.graph.models.DateTimeTimeZone;
import com.microsoft.graph.models.EmailAddress;
import com.microsoft.graph.models.Event;
import com.microsoft.graph.models.ItemBody;
import com.microsoft.graph.models.Location;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.CalendarCollectionPage;
import com.microsoft.graph.requests.CalendarGetScheduleCollectionPage;
import com.microsoft.graph.requests.EventCollectionPage;
import com.microsoft.graph.requests.GraphServiceClient;
import com.microsoft.graph.requests.UserCollectionPage;
import com.tarion.graphservice.dto.AvailableTimeslotsResponse;
import com.tarion.graphservice.dto.AvailableTwoFcrTimeslotsResponse;
import com.tarion.graphservice.dto.CreateAppointmentRequest;
import com.tarion.graphservice.dto.DeleteAppointmentRequest;
import com.tarion.graphservice.dto.FcrInfoDto;
import com.tarion.graphservice.dto.Timeslot;
import com.tarion.graphservice.dto.TwoFcrTimeslot;
import com.tarion.graphservice.util.GraphServiceConstants;
import com.tarion.graphservice.util.TimeslotUtil;

import lombok.extern.java.Log;

@Log
public class GraphService {
	

	private static final String EASTERN_STANDARD_TIME_ZONE = "Eastern Standard Time";
	private static String CLIENT_ID = "2c30bd67-8c49-4f4e-b466-060ac709c4b2";
    private static String USER_ID = "562cf2c9-5e3b-4ffe-8508-f3423a522bc7";
    private static String TENANT_GUID = "24145aeb-c917-40e8-9bfc-a675579170e8"; 
    private static String CLIENT_SECRET = "Sm47Q~vfWwUdYEt42eAvAjHs7Wab1sFAxdT7E"; 
    private static List<String> SCOPES = Arrays.asList("https://graph.microsoft.com/.default");
	public static final String DATE_PATTERN_DATE_ONLY = "yyyy-MM-dd";
	
	public  Map<String, FcrInfoDto> fcrInfoMapByEmail = new HashMap<>();
	public  Map<String, FcrInfoDto> fcrInfoMapByUsername = new HashMap<>();
	
	public GraphService() {
		FcrInfoDto dto = new FcrInfoDto();
		dto.setFrcEmailAddress("oagady@GraphOagady.onmicrosoft.com");
		dto.setFcrUsername("FCR1");
		dto.setFcrGraphId("562cf2c9-5e3b-4ffe-8508-f3423a522bc7");
		fcrInfoMapByEmail.put("oagady@GraphOagady.onmicrosoft.com", dto);
		
		dto = new FcrInfoDto();
		dto.setFrcEmailAddress("FCR2@graphoagady.onmicrosoft.com");
		dto.setFcrUsername("FCR2");
		dto.setFcrGraphId("b03248c3-2bc5-4cda-bd1f-5c9928389056");
		fcrInfoMapByEmail.put("FCR2@graphoagady.onmicrosoft.com", dto);
		
		log.log(Level.INFO, "Created fcrInfoMapByEmail" + fcrInfoMapByEmail);
		
		dto = new FcrInfoDto();
		dto.setFrcEmailAddress("oagady@GraphOagady.onmicrosoft.com");
		dto.setFcrUsername("FCR1");
		dto.setFcrGraphId("562cf2c9-5e3b-4ffe-8508-f3423a522bc7");
		fcrInfoMapByUsername.put("FCR1", dto);
		
		dto = new FcrInfoDto();
		dto.setFrcEmailAddress("FCR2@graphoagady.onmicrosoft.com");
		dto.setFcrUsername("FCR2");
		dto.setFcrGraphId("b03248c3-2bc5-4cda-bd1f-5c9928389056");
		fcrInfoMapByUsername.put("FCR2", dto);
		log.log(Level.INFO, "Created fcrInfoMapByUsername" + fcrInfoMapByUsername);
	}

	public  EventCollectionPage getEvents(String fcrUsername, String startDateTime, String endDateTime) {
		log.log(Level.INFO, "getEvents: " + fcrUsername + ", " + startDateTime + ", " + endDateTime);

		GraphServiceClient graphClient = authenticateUser();
		EventCollectionPage events = null;
		
		try {
			LinkedList<Option> requestOptions = new LinkedList<Option>();
			requestOptions.add(new QueryOption("startDateTime", "2022-02-02T00:30:00.0000000"));
			requestOptions.add(new QueryOption("endDateTime", "2022-02-03T00:30:00.0000000"));
			requestOptions.add(new HeaderOption("Prefer", "outlook.timezone=\"Eastern Standard Time\""));
		

			events = graphClient.users().byId(getFcrGraphIdByUsername(fcrUsername)).calendar().events()
				    .buildRequest(requestOptions)
				    .select("subject,body,start,end,isAllDay,calendar")
				    .get();

			
		} catch (Exception e) {
			log.log(Level.INFO, "Error in getEvents method: " + e.getMessage() );
			e.printStackTrace();
		}
		log.log(Level.INFO, "getEvents end: " + events);
		return events;
	}
	
	public  CalendarGetScheduleCollectionPage getSchedules(String fcrUsername, String startDateTime, String endDateTime) {
		log.log(Level.INFO, "getSchedules: " + fcrUsername + ", " + startDateTime + ", " + endDateTime);
		List<Timeslot> timeslots = new ArrayList<>();
		GraphServiceClient graphClient = authenticateUser();		
		CalendarGetScheduleCollectionPage schedules = null;		
		try {
			LinkedList<Option> requestOptions = getRequestOptions();		
			
			DateTimeTimeZone start = new DateTimeTimeZone();
			start.dateTime = startDateTime;
			start.timeZone = EASTERN_STANDARD_TIME_ZONE;

			DateTimeTimeZone end = new DateTimeTimeZone();
			end.dateTime = endDateTime;
			end.timeZone = EASTERN_STANDARD_TIME_ZONE;
			
			LinkedList<String> emailAddresses = new LinkedList<String>();
			emailAddresses.add(getFcrEmailByUsername(fcrUsername));
			log.log(Level.INFO, "email addresses: " + emailAddresses);
			int availabilityViewInterval = 60;

			schedules = graphClient.users().byId(getFcrGraphIdByUsername(fcrUsername)).calendar()
				.getSchedule(CalendarGetScheduleParameterSet
					.newBuilder()
					.withSchedules(emailAddresses)
					.withEndTime(end)
					.withStartTime(start)
					.withAvailabilityViewInterval(availabilityViewInterval)
					.build())
				.buildRequest( requestOptions )
				.post();

			log.log(Level.INFO, "Schedules: " + schedules);
		} catch (Exception e) {
			log.log(Level.INFO, "Error in getEvents: " + e.getMessage() );
			e.printStackTrace();
		}
		log.log(Level.INFO, "getSchedules end");
		return schedules;
	}
	
	
	public  AvailableTimeslotsResponse getAvailableTimeslots(List<String> fcrUsernames, String startTimeString, String endTimeString, 
			String appointmentType, boolean multipleFCRs) {
		log.log(Level.INFO, "getAvailableTimeslots: " + fcrUsernames);
		List<Timeslot> inspectionsList  = null;
		List<Timeslot> allTimeslots = new ArrayList<>();
		List<Timeslot> availableTimeslots = new ArrayList<>();
		if(appointmentType.equals(GraphServiceConstants.TWO_FCR_TIMESLOT)) {
			
		}
		for(String fcrUsername : fcrUsernames) {
				log.log(Level.INFO, "getAvailableTimeslots fcrUsername: " + fcrUsername);
				try {
					EventCollectionPage events = getCalendarView(fcrUsername, startTimeString, endTimeString);
					 if(appointmentType.equals(GraphServiceConstants.AM_PM_TIMESLOT)) {
						 //inspection list is only needed for AM/PM timeslots to compare driving time between AM and PM inspections
						 inspectionsList  = new TimeslotUtil().getInspectionsList(events, fcrUsername);
						 availableTimeslots = new TimeslotUtil().getAvailableAMPMTimeslots(fcrUsername, startTimeString, endTimeString, events);						 
					 }else if (appointmentType.equals(GraphServiceConstants.ALL_DAY_TIMESLOT)){
						 availableTimeslots = new TimeslotUtil().getAvailableAllDaysTimeslots(fcrUsername, startTimeString, endTimeString, events);						 
					 }else if (appointmentType.equals(GraphServiceConstants.MULTIPLE_DAYS_TIMESLOT)){
						 availableTimeslots = new TimeslotUtil().getAvailableMultipleDaysTimeslots(fcrUsername, startTimeString, endTimeString, events);						 
					 }
					 allTimeslots.addAll(availableTimeslots);			
				} catch (Exception e) {
					log.log(Level.INFO, "error in getAvailableTimeslots: " + e.getMessage());
					e.printStackTrace();
				}
		}		
		AvailableTimeslotsResponse response = new AvailableTimeslotsResponse();
		response.setAvailabletimeslots(allTimeslots);
		response.setInspectionsList(inspectionsList);		
		//log.log(Level.INFO, "AvailableTimeslotsResponse: " + response);
			
		return response;
	}
	
	public  AvailableTwoFcrTimeslotsResponse getAvailableTwoFcrTimeslots(List<String> fcrUsernames, String startTimeString, String endTimeString, 
			String appointmentType, boolean multipleFCRs) {
		log.log(Level.INFO, "getAvailableTwoFcrTimeslots: " + fcrUsernames);
		List<Timeslot> availableTimeslots = new ArrayList<>();
		Map<String, List<Timeslot>> availableTimeslotsAllFcr = new HashMap<>();

		for(String fcrUsername : fcrUsernames) {
			EventCollectionPage events = getCalendarView(fcrUsername, startTimeString, endTimeString);
			availableTimeslots = new TimeslotUtil().getAvailableAllDaysTimeslots(fcrUsername, startTimeString, endTimeString, events);	
			availableTimeslotsAllFcr.put(fcrUsername, availableTimeslots);
		}	
		List<TwoFcrTimeslot> allTwoFcrTimeslots = processTwoFcrTimeslots(availableTimeslotsAllFcr, 0);   
		
		AvailableTwoFcrTimeslotsResponse response = new AvailableTwoFcrTimeslotsResponse();
		response.setAvailabletimeslots(allTwoFcrTimeslots);

		return response;
	}
	

	private List<TwoFcrTimeslot> processTwoFcrTimeslots(Map<String, List<Timeslot>> availableTimeslotsAllFcr, int offset){

		List<TwoFcrTimeslot> twoFcrTimeslots = new ArrayList<>();
		
		List<String> keys = new ArrayList<>(availableTimeslotsAllFcr.keySet());		
		while(offset < availableTimeslotsAllFcr.size() - 1) {
			String keyFcr1 = keys.get(offset);
			String keyFcr2 = keys.get(offset + 1);				
			List<Timeslot> availableTimeslotsFcr1 = availableTimeslotsAllFcr.get(keyFcr1);
			List<Timeslot> availableTimeslotsFcr2 = availableTimeslotsAllFcr.get(keyFcr2);
				twoFcrTimeslots.addAll(findCommonAvailableTimeslotsForBothFcrs(availableTimeslotsFcr1, availableTimeslotsFcr2));
			offset++;
		}
				
		return twoFcrTimeslots;
	}

	private List<TwoFcrTimeslot> findCommonAvailableTimeslotsForBothFcrs(List<Timeslot> availableTimeslotsFcr1,
			List<Timeslot> availableTimeslotsFcr2) {
		List<TwoFcrTimeslot> availableTimeslotsForBothFcrs = new ArrayList<>();
		for(Timeslot timeslotFcr1 : availableTimeslotsFcr1) {
			for(Timeslot timeslotFcr2 : availableTimeslotsFcr2) {
				if(isSameTimeslot(timeslotFcr1, timeslotFcr2)) {
					TwoFcrTimeslot twoFcrTimeslot = createTwoFcrTimeslot(timeslotFcr1, timeslotFcr2);					
					availableTimeslotsForBothFcrs.add(twoFcrTimeslot);
				}
			}
		}
		
		return null;
	}

	private TwoFcrTimeslot createTwoFcrTimeslot(Timeslot timeslotFcr1, Timeslot timeslotFcr2) {
		TwoFcrTimeslot twoFcrTimeslot = new TwoFcrTimeslot();
		twoFcrTimeslot.setAppointmentType(GraphServiceConstants.TWO_FCR_TIMESLOT);
		twoFcrTimeslot.setStarttime(timeslotFcr1.getStarttime());
		twoFcrTimeslot.setEndtime(timeslotFcr1.getEndtime());
		twoFcrTimeslot.setFcr1EmailAddress(timeslotFcr1.getEmailAddress());
		twoFcrTimeslot.setFcr2EmailAddress(timeslotFcr2.getEmailAddress());
		twoFcrTimeslot.setFcr1Username(timeslotFcr1.getFcrid());
		twoFcrTimeslot.setFcr2Username(timeslotFcr2.getFcrid());
		twoFcrTimeslot.setEnrolmentAddress("TODO");
		return twoFcrTimeslot;
	}

	private boolean isSameTimeslot(Timeslot timeslotFcr1, Timeslot timeslotFcr2) {
		if(timeslotFcr1.getStarttime().isEqual(timeslotFcr2.getStarttime())) {
			return true;
		}
		return false;
	}

	public  EventCollectionPage getCalendarView(String fcrUsername, String startTimeString, String endTimeString) {
		log.log(Level.INFO, "getCalendarView start, fcr usernames: " + fcrUsername + ", " + startTimeString + ", " + endTimeString);
		GraphServiceClient graphClient = authenticateUser();		
		EventCollectionPage  calendarView = null;
		
		try {
			log.log(Level.INFO, "Getting calendar view");
			
			LinkedList<Option> requestOptions = new LinkedList<Option>();
			
			DateTimeTimeZone start = new DateTimeTimeZone();
			start.dateTime = startTimeString;
			start.timeZone = EASTERN_STANDARD_TIME_ZONE;

			DateTimeTimeZone end = new DateTimeTimeZone();
			end.dateTime = endTimeString;
			end.timeZone = EASTERN_STANDARD_TIME_ZONE;
			
			
			requestOptions.add(new QueryOption("startDateTime", startTimeString));
			requestOptions.add(new QueryOption("endDateTime", endTimeString));
			requestOptions.add(new HeaderOption("Prefer", "outlook.timezone=\"Eastern Standard Time\""));
			requestOptions.add(new HeaderOption("Prefer", "outlook.body-content-type=\"text\""));
			
			
			calendarView = graphClient.users().byId(getFcrGraphIdByUsername(fcrUsername)).calendar()
				.calendarView()
				//.with
				.buildRequest( requestOptions )
				.get();
			log.log(Level.INFO, "Calendar view: " + calendarView);
		} catch (Exception e) {
			log.log(Level.INFO, "Error in getEvents method: " + e.getMessage() );
			e.printStackTrace();
		}
		log.log(Level.INFO, "getEvents end");
		//return timeslotsMap;
		return calendarView;
	}


	public UserCollectionPage getUsers() {
		log.log(Level.INFO, "getUsers start");
		GraphServiceClient graphClient = authenticateUser();
    	LinkedList<Option> requestOptions = getRequestOptions();
    	
    	

		final UserCollectionPage users = graphClient.users()
				    .buildRequest(requestOptions)
				    .get();
		log.log(Level.INFO, "getUsers end : " + users);
		return users;
	}
	
	public CalendarCollectionPage getCalendars() {
		log.log(Level.INFO, "getCalendars called");
		GraphServiceClient graphClient = authenticateUser();
    	LinkedList<Option> requestOptions = getRequestOptions();	
    	
    	CalendarCollectionPage calendars = graphClient.users("562cf2c9-5e3b-4ffe-8508-f3423a522bc7").calendars()
    	.buildRequest()
    	.get();
    	log.log(Level.INFO, "Returning calendars: " + calendars);
    	return calendars;
	}
	
	public Event createAppointment(CreateAppointmentRequest request) {
		log.log(Level.INFO, "postEvent CreateAppointmentRequest: " + request);
		GraphServiceClient graphClient = authenticateUser();
    	LinkedList<Option> requestOptions = getRequestOptions();
    	Event newEvent = createNewEvent(request);
    	String userGraphId = fcrInfoMapByUsername.get(request.getFcrId()).getFcrGraphId();
    	Event event = graphClient.users(userGraphId).events()
    	.buildRequest(requestOptions)
    	.post(newEvent);
   	
    	return event;
	}


	public void deleteEvent(DeleteAppointmentRequest request) {
		log.log(Level.INFO, "GraphService: DeleteAppointmentRequest: " + request);
		try {
			GraphServiceClient graphClient = authenticateUser();
			LinkedList<Option> requestOptions = getRequestOptions();			
			FcrInfoDto fcrInfoDto =  fcrInfoMapByUsername.get(request.getFcrUsername());
			String userId = fcrInfoDto.getFcrGraphId();
			graphClient.users(userId).events(request.getGraphAppointmentId())
			.buildRequest(requestOptions)
			.delete();
		} catch (ClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	String resustl = "Success";
	}
	
	
    
    public GraphServiceClient  authenticateUser() {
    	log.log(Level.INFO, "authenticateUser");
    	ClientSecretCredential clientSecretCredential = null;
		try {
			clientSecretCredential = new ClientSecretCredentialBuilder()
			        .clientId(CLIENT_ID)
			        .clientSecret(CLIENT_SECRET)
			        .tenantId(TENANT_GUID)
			        .build();
		} catch (Exception e) {
			log.log(Level.INFO, "Getting clientSecretCredential error: " + e.getMessage());
		}

    	GraphServiceClient graphClient = null;
		try {
			final TokenCredentialAuthProvider tokenCredAuthProvider =
			        new TokenCredentialAuthProvider(SCOPES, clientSecretCredential);
			log.log(Level.INFO, "Getting GraphServiceClient");
			graphClient = GraphServiceClient
			        .builder()
			        .authenticationProvider(tokenCredAuthProvider)
			        .buildClient();
		} catch (Exception e) {
			log.log(Level.INFO, "Getting GraphServiceClient error: " + e.getMessage());
		}
    	
    	return graphClient;
    }

	private String formatTime(DateTimeTimeZone timestamp) {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN_DATE_ONLY);
		return formatter.format(timestamp);
	}

	private LinkedList<Option> getRequestOptions() {
		LinkedList<Option> requestOptions = new LinkedList<Option>();
    	requestOptions.add(new HeaderOption("Prefer", "outlook.timezone=\"Eastern Standard Time\""));
		return requestOptions;
	}
	
	private List<TwoFcrTimeslot>  processTwoFcrTimeslots( List<List<TwoFcrTimeslot>> availableTimeslotsAllFcr){
		List<TwoFcrTimeslot> allTimeslots = new ArrayList<>();
		
		return allTimeslots;
		
	}

	private Event createNewEvent(CreateAppointmentRequest request) {
		log.log(Level.INFO, "createNewEvent: " + request);
		Event event = new Event();
		event.subject = request.getTopic();
		ItemBody body = new ItemBody();
		body.contentType = BodyType.HTML;
		body.content = request.getBody();
		event.body = body;
		DateTimeTimeZone start = new DateTimeTimeZone();
		start.dateTime = request.getStartDateTimeStr();
		start.timeZone = EASTERN_STANDARD_TIME_ZONE;
		event.start = start;
		DateTimeTimeZone end = new DateTimeTimeZone();
		end.dateTime = request.getEndDateTimeStr();
		end.timeZone = EASTERN_STANDARD_TIME_ZONE;
		event.end = end;
		Location location = new Location();
		location.displayName = "Online";
		event.location = location;
		LinkedList<Attendee> attendeesList = new LinkedList<Attendee>();
		Attendee attendees = new Attendee();
		EmailAddress emailAddress = new EmailAddress();
		emailAddress.address =  "test@test.com";
		emailAddress.name = request.getFcrId();
		attendees.emailAddress = emailAddress;
		attendees.type = AttendeeType.REQUIRED;
		attendeesList.add(attendees);
		event.attendees = attendeesList;
		return event;
	}

	public FcrInfoDto getFrcFcrInfoDto(String emailAddress) {
		return fcrInfoMapByEmail.get(emailAddress);
	}
	
	public String  getFrcFcrGraphUserId(String emailAddress) {
		FcrInfoDto fcrInfoDto =  fcrInfoMapByEmail.get(emailAddress);
		return fcrInfoDto.getFcrGraphId();
	}
	
	public String  getFrcUsername(String emailAddress) {
		FcrInfoDto fcrInfoDto =  fcrInfoMapByEmail.get(emailAddress);
		return fcrInfoDto.getFcrUsername();
	}
	
	public String  getFcrGraphIdByUsername(String fcrId) {
		FcrInfoDto fcrInfoDto =  fcrInfoMapByUsername.get(fcrId);
		return fcrInfoDto.getFcrGraphId();
	}
	
	public String  getFcrEmailByUsername(String fcrId) {
		FcrInfoDto fcrInfoDto =  fcrInfoMapByUsername.get(fcrId);
		return fcrInfoDto.getFrcEmailAddress();
	}
	
}

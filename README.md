# Local Development
For local development the application must be started with the spring profile `dev`. Properties for this profile are separately managed in `/src/main/resources/application-dev.properties`.

These properties will be injected using a ConfigMap on Kubernetes when deployed.

The CM libraries need CM properties files to work, they are in the folder cm-properties. 
Set properties.path in application-dev.yml to the correct path for the environment you want to use (currently dev5). 

Maven usage: `mvn spring-boot:run -Dspring-boot.run.profiles=dev`

# Usage of Jib
`mvn compile jib:dockerBuild` will publish to your local docker daemon

`mvn compile jib:build` - will build and push the image to the repository defined in the pom. Correct credentials must be set (usually only use as part of a Jenkins build)